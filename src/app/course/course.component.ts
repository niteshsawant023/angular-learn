import { CoursesService } from './../courses.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  email;
  text = "This is a very long random text field.bla bla bla bla";
  constructor() { 
  }
  ngOnInit() {
  }

  onButtonClick(){

    console.log(this.email);
    
  }
}
