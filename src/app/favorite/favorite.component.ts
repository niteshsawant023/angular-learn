import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  @Input('is-fav') isEmpty;
  @Output('change') click = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.isEmpty = !this.isEmpty;
    this.click.emit({
      newValue: this.isEmpty
    });
  }



}

export interface favEventArgsInterface {
  newValue: boolean
}