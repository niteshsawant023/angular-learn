import { Component } from '@angular/core';
import { favEventArgsInterface } from './favorite/favorite.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  post = {
    'isFav':false,
  }

  tweet = {
    body:'...',
    isActive:true,
    likeCount:10
  }

  courses = [1,2];

  onFavChanged(eventArgs:favEventArgsInterface) {
    console.log(eventArgs);

  }
}
